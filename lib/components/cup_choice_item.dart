import 'package:flutter/material.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem({
    super.key,
    required this.pandon,
    required this.currentMoney
  });

  final num pandon; //100
  final num currentMoney; //0

  @override
  State<CupChoiceItem> createState() => _CupChoiceItemState();
}

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc = 'assets/cup.jpg';

  // _isOpen이 아닐 경우 ture로 바꿔줘라
  void _calculateStart() {
    if (!_isOpen) {
      setState(() {
        _isOpen = true;
      });
    }
  }


  // 상자 열었을 때
  // 판돈이 현재 돈 보다 적을 때 : 꽝 이미지
  // 판돈이 현재 돈과 같을 때 : 100원 이미지
  // 판돈이 현재 보다 많을 때 : 당첨 이미지

  // 상자를 안 열었을 때 무조건 컵 이미지

  void _calculateImgSrc() {
    String tempImgSrc = '';
    if (_isOpen) {
      if (widget.pandon > widget.currentMoney) {
        tempImgSrc = 'assets/bomb.jpg';
      } else if (widget.pandon == widget.currentMoney) {
        tempImgSrc = 'assets/won.png';
      } else {
        tempImgSrc = 'assets/dance.jpg';
      }
    } else {
      tempImgSrc = 'assets/cup.jpg';
    }

    setState(() {
      imgSrc = tempImgSrc;
    });
  }


  @override
  Widget build(BuildContext context) {
    // 제스쳐를 감지
    return GestureDetector(
      onTap: () {
        _calculateStart();
        _calculateImgSrc();
      },
      child: Container(
        margin: EdgeInsets.fromLTRB(5, 100, 0, 0),
        child: SizedBox(
          width: 130,
          height: 130,
          child: Image.asset(imgSrc),
        ),
      )
    );
  }
}
